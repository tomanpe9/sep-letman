package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.common.Clock;
import eu.profinit.education.flightlog.dao.ClubDatabaseDao;
import eu.profinit.education.flightlog.domain.entities.Airplane;
import eu.profinit.education.flightlog.domain.repositories.FlightRepository;
import eu.profinit.education.flightlog.domain.repositories.PersonRepository;
import eu.profinit.education.flightlog.exceptions.ValidationException;
import eu.profinit.education.flightlog.service.FlightServiceImpl;
import eu.profinit.education.flightlog.domain.repositories.ClubAirplaneRepository;
import eu.profinit.education.flightlog.IntegrationTestConfig;
import eu.profinit.education.flightlog.domain.repositories.ClubAirplaneRepository;
import eu.profinit.education.flightlog.domain.entities.Flight;
import eu.profinit.education.flightlog.to.AirplaneTo;
import eu.profinit.education.flightlog.to.AirplaneWithCrewTo;
import eu.profinit.education.flightlog.to.FlightTakeoffTo;
import eu.profinit.education.flightlog.to.PersonTo;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationTestConfig.class)
@Transactional
@TestPropertySource(
    locations = "classpath:application-integrationtest.properties")
public class FlightServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Mock
    private ClubDatabaseDao clubDatabaseDao;

    private PersonServiceImpl personService;

    @Autowired
    private Clock clock;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private ClubAirplaneRepository clubAirplaneRepository;

    private FlightServiceImpl flightService;

    @Before
    public void setUp(){
        this.personService = new PersonServiceImpl(personRepository, clubDatabaseDao);
        this.flightService = new FlightServiceImpl(flightRepository, clubAirplaneRepository, clock, personService);
    }

    @Test(expected = ValidationException.class)
    public void testTakeOff() {
        AirplaneTo towplane = AirplaneTo.builder().id(5L).immatriculation("OK125").type("Type engine").build();
        AirplaneWithCrewTo towplaneWithCrew = AirplaneWithCrewTo.builder().airplane(towplane).note("Note towplane").pilot(PersonTo.builder().firstName("Adalbert").lastName("Kolínsk7").memberId(123L).build()).build();
        AirplaneTo glider = AirplaneTo.builder().id(6L).immatriculation("OKHDG").type("Type glider").build();
        AirplaneWithCrewTo gliderWithCrew = AirplaneWithCrewTo.builder().airplane(glider).note("Note glider").pilot(PersonTo.builder().firstName("Eliška").lastName("Kutnohorská").memberId(125L).build()).build();

        FlightTakeoffTo start = FlightTakeoffTo.builder().task("Task A").towplane(towplaneWithCrew).glider(gliderWithCrew).takeoffTime(null).build();

        flightService.takeoff(start);
    }

    @Test()
    public void testCreateGliderFlight() {
        AirplaneTo towplane = AirplaneTo.builder().id(5L).immatriculation("OK125").type("Type engine").build();
        AirplaneWithCrewTo towplaneWithCrew = AirplaneWithCrewTo.builder().airplane(towplane).note("Note towplane").pilot(PersonTo.builder().firstName("Adalbert").lastName("Kolínsk7").memberId(123L).build()).build();

        FlightTakeoffTo start = FlightTakeoffTo.builder().task("Task A").towplane(towplaneWithCrew).glider(null).takeoffTime(null).build();

        Flight flight = flightService.createGliderFlight(start);
        assertEquals("Flight wihout glider should be null", null, flight);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAirplane() {
        AirplaneTo towplane = AirplaneTo.builder().id(5L).build();
        Airplane airplane = flightService.getAirplane(towplane);
    }

}
