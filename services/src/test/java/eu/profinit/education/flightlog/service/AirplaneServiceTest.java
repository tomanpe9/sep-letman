package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.service.AirplaneServiceImpl;
import eu.profinit.education.flightlog.domain.repositories.ClubAirplaneRepository;
import eu.profinit.education.flightlog.IntegrationTestConfig;
import eu.profinit.education.flightlog.domain.repositories.ClubAirplaneRepository;
import eu.profinit.education.flightlog.domain.entities.Flight;
import eu.profinit.education.flightlog.to.AirplaneTo;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationTestConfig.class)
@Transactional
@TestPropertySource(
    locations = "classpath:application-integrationtest.properties")
public class AirplaneServiceTest {

    @Autowired
    private ClubAirplaneRepository clubAirplaneRepository;

    private AirplaneService airplaneService;

    @Before
    public void setUp(){
        this.airplaneService = new AirplaneServiceImpl(this.clubAirplaneRepository);
    }

    @Test()
    public void testGetClubAirplanes() {
        List<AirplaneTo> clubAirplanes = airplaneService.getClubAirplanes();
        assertEquals("There should be 2 club airplanes", 2, clubAirplanes.size());
    }

}
