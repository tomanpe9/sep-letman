package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.domain.entities.Flight;
import eu.profinit.education.flightlog.domain.repositories.FlightRepository;
import eu.profinit.education.flightlog.to.FileExportTo;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

@Service
public class CsvExportServiceImpl implements CsvExportService {

    private final FlightRepository flightRepository;

    private final String fileName;

    public CsvExportServiceImpl(FlightRepository flightRepository, @Value("${csv.export.flight.fileName}") String fileName) {
        this.flightRepository = flightRepository;
        this.fileName = fileName;
    }

    @Override
    public FileExportTo getAllFlightsAsCsv() {
        List<Flight> flights = flightRepository.findAllByLandingTimeIsNotNullOrderByTakeoffTimeAscIdAscFlightTypeDesc();
        byte[] content = processCsvData(flights).getBytes();
        return new FileExportTo(fileName, MediaType.MULTIPART_FORM_DATA, content);
    }

    public String processCsvData(List<Flight> flights) {
        StringBuilder builder = new StringBuilder();
        try (CSVPrinter csvPrinter = new CSVPrinter(builder, CSVFormat.EXCEL);) {
            csvPrinter.printRecords(flights);
        } catch (IOException e) {
            throw new IllegalStateException("failed loading data");
        }
        return builder.toString();
    }

}
