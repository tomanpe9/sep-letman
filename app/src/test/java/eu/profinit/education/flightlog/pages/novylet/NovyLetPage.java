package eu.profinit.education.flightlog.pages.novylet;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class NovyLetPage extends ParentPage {

    private WebElement casVzletu;
    private WebElement uloha;
    private WebElement start;

    private VlecnaPage vlecnaPage;
    private KluzakPage kluzakPage;

    public NovyLetPage(WebDriver driver) {
        super(driver);

        casVzletu = driver.findElement(By.xpath("//*[@id=\"takeoffTime\"]"));
        uloha = driver.findElement(By.xpath("//*[@id=\"task\"]"));
        start = driver.findElement(By.xpath("/html/body/router-view/div/form/div[2]/div/button"));
    }

    public NovyLetPage fillCasVzletu(String keys) {

        casVzletu.sendKeys(keys);

        return this;
    }

    public NovyLetPage fillUloha(String keys) {

        uloha.sendKeys(keys);

        return this;
    }

    public NovyLetPage clickStart() {

        start.click();

        return this;
    }

    public VlecnaPage getVlecnaPage() {

        if (vlecnaPage == null) {
            vlecnaPage = new VlecnaPage(driver, this);
        }

        return vlecnaPage;
    }

    public KluzakPage getKluzakPage() {

        if (kluzakPage == null) {
            kluzakPage = new KluzakPage(driver, this);
        }

        return kluzakPage;
    }

    public NovyLetPage acceptAlert() {
        int numTries = 5;

        while (true) {
            try {
                TimeUnit.SECONDS.sleep(1);
                driver.switchTo().alert().accept();
                break;
            } catch (NoAlertPresentException e) {
                if (--numTries < 1) throw e;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return this;
    }
}
