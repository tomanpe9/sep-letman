package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class KluzakSoukromePage extends ChildKluzakPage {

    private WebElement imatrikulace;
    private WebElement typ;

    public KluzakSoukromePage(WebDriver driver, KluzakPage parent) {
        super(driver, parent);

        imatrikulace = driver.findElement(By.xpath("//*[@id=\"guestAirplaneImmatriculation\"]"));
        typ = driver.findElement(By.xpath("//*[@id=\"guestAirplaneType\"]"));
    }

    public KluzakSoukromePage fillImatrikulace(String keys) {

        imatrikulace.sendKeys(keys);

        return this;
    }

    public KluzakSoukromePage fillTyp(String keys) {

        typ.sendKeys(keys);

        return this;
    }
}
