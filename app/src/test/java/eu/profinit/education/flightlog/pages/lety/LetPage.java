package eu.profinit.education.flightlog.pages.lety;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LetPage extends ParentPage {

    private LetyPage parent;

    private WebElement vybratCasPristani;
    private WebElement pristat;

    private CasPristaniPage casPristaniPage;

    public LetPage(WebDriver driver, LetyPage parent, WebElement tableBody) {
        super(driver);

        this.parent = parent;

        vybratCasPristani = tableBody.findElement(By.xpath("/html/body/router-view/div/div/div/table/tbody/tr/td[6]/a[1]/i"));
        pristat = tableBody.findElement(By.xpath("/html/body/router-view/div/div/div/table/tbody/tr/td[6]/a[2]/i"));
    }

    public LetyPage getParent() {
        return parent;
    }

    public LetPage clickVybratCasPristani() {

        vybratCasPristani.click();

        if (casPristaniPage == null) {
            casPristaniPage = new CasPristaniPage(driver, this);
        }

        return this;
    }

    public CasPristaniPage getCasPristaniPage() {

        return casPristaniPage;
    }

    public LetPage clickPristat() {

        pristat.click();

        return this;
    }
}
