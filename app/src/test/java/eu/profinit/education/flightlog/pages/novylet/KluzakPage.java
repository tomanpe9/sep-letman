package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class KluzakPage extends ChildNovyLetPage{

    private NovyLetPage parent;

    private WebElement klubove;
    private KluzakKlubovePage klubovePage;

    private WebElement soukrome;
    private KluzakSoukromePage soukromePage;

    private WebElement zadne;
    private KluzakZadnePage zadnePage;

    private WebElement clenKlubu;
    private KluzakClenKlubuPage clenKlubuPage;

    private WebElement host;
    private KluzakHostPage hostPage;

    private Select kopilot;
    private WebElement poznamka;

    public KluzakPage(WebDriver driver, NovyLetPage parent) {
        super(driver, parent);

        this.parent = parent;

        klubove = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/compose/div[1]/div/div/label[1]"));
        soukrome = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/compose/div[1]/div/div/label[2]"));
        zadne = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/compose/div[1]/div/div/label[3]"));

        clenKlubu = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/div[2]/compose/div[2]/div/div/label[1]"));

        host = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/div[2]/compose/div[2]/div/div/label[2]"));

        kopilot = new Select(driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/div[2]/div/div[1]/select")));
        poznamka = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/div[2]/div/div[2]/input"));
    }

    public NovyLetPage getParent() {
        return parent;
    }

    public KluzakPage clickKlubove() {

        klubove.click();

        if (klubovePage == null) {
            klubovePage = new KluzakKlubovePage(driver, this);
        }

        return this;
    }

    public KluzakKlubovePage getKlubovePage() {
        return klubovePage;
    }

    public KluzakPage clickSoukrome() {

        soukrome.click();

        if (soukromePage == null) {
            soukromePage = new KluzakSoukromePage(driver, this);
        }

        return this;
    }

    public KluzakSoukromePage getSoukromePage() {
        return soukromePage;
    }

    public KluzakPage clickZadne() {

        zadne.click();

        if (zadnePage == null) {
            zadnePage = new KluzakZadnePage(driver, this);
        }

        return this;
    }

    public KluzakZadnePage getZadnePage() {
        return zadnePage;
    }

    public KluzakPage clickClenKlubu() {

        clenKlubu.click();

        if (clenKlubuPage == null) {
            clenKlubuPage = new KluzakClenKlubuPage(driver, this);
        }

        return this;
    }

    public KluzakClenKlubuPage getClenKlubuPage() {
        return clenKlubuPage;
    }

    public KluzakPage clickHost() {

        host.click();

        if (hostPage == null) {
            hostPage = new KluzakHostPage(driver, this);
        }

        return this;
    }

    public KluzakHostPage getHostPage() {
        return hostPage;
    }

    public KluzakPage selectKopilot(int index) {

        kopilot.selectByIndex(index);

        return this;
    }

    public KluzakPage fillPoznamka(String keys) {

        poznamka.sendKeys(keys);

        return this;
    }
}
