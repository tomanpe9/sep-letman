package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class VlecnaPage extends ChildNovyLetPage {

    private NovyLetPage parent;

    private WebElement klubove;
    private VlecnaKlubovePage klubovePage;

    private WebElement soukrome;
    private VlecnaSoukromePage soukromePage;

    private WebElement clenKlubu;
    private VlecnaClenKlubuPage clenKlubuPage;

    private WebElement host;
    private VlecnaHostPage hostPage;

    private Select kopilot;
    private WebElement poznamka;

    public VlecnaPage(WebDriver driver, NovyLetPage parent) {
        super(driver, parent);

        this.parent = parent;

        klubove = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/compose/div[1]/div/div/label[1]"));
        soukrome = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/compose/div[1]/div/div/label[2]"));

        clenKlubu = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[2]/div/div/label[1]"));

        host = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[2]/div/div/label[2]"));

        kopilot = new Select(driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/div/div[1]/select")));
        poznamka = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/div/div[2]/input"));

    }

    public NovyLetPage getParent() {
        return parent;
    }

    public VlecnaPage clickKlubove() {

        klubove.click();

        if (klubovePage == null) {
            klubovePage = new VlecnaKlubovePage(driver, this);
        }

        return this;
    }

    public VlecnaKlubovePage getKlubovePage() {
        return klubovePage;
    }

    public VlecnaPage clickSoukrome() {

        soukrome.click();

        if (soukromePage == null) {
            soukromePage = new VlecnaSoukromePage(driver, this);
        }

        return this;
    }

    public VlecnaSoukromePage getSoukromePage() {
        return soukromePage;
    }

    public VlecnaPage clickClenKlubu() {

        clenKlubu.click();

        if (clenKlubuPage == null) {
            clenKlubuPage = new VlecnaClenKlubuPage(driver, this);
        }

        return this;
    }

    public VlecnaClenKlubuPage getClenKlubuPage() {
        return clenKlubuPage;
    }

    public VlecnaPage clickHost() {

        host.click();

        if (hostPage == null) {
            hostPage = new VlecnaHostPage(driver, this);
        }

        return this;
    }

    public VlecnaHostPage getHostPage() {
        return hostPage;
    }

    public VlecnaPage selectKopilot(int index) {

        kopilot.selectByIndex(index);

        return this;
    }

    public VlecnaPage fillPoznamka(String keys) {

        poznamka.sendKeys(keys);

        return this;
    }
}
