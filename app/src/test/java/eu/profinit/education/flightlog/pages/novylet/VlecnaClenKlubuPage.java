package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class VlecnaClenKlubuPage extends ChildVlecnaPage {

    private Select pilot;

    public VlecnaClenKlubuPage(WebDriver driver, VlecnaPage parent) {
        super(driver, parent);

        pilot = new Select(driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div/select")));
    }

    public VlecnaClenKlubuPage selectPilot(int index) {

        pilot.selectByIndex(index);

        return this;
    }
}
