package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class VlecnaHostPage extends ChildVlecnaPage {

    private WebElement jmeno;
    private WebElement prijmeni;
    private WebElement ulice;
    private WebElement mesto;
    private WebElement PSC;
    private WebElement zeme;

    public VlecnaHostPage(WebDriver driver, VlecnaPage parent) {
        super(driver, parent);

        jmeno = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div[1]/input"));
        prijmeni = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div[2]/input"));
        ulice = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div[3]/input"));
        mesto = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div[4]/input"));
        PSC = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div[5]/input"));
        zeme = driver.findElement(By.xpath("/html/body/router-view/div/form/compose[1]/div[2]/compose/div[3]/div[6]/input"));
    }

    public VlecnaHostPage fillJmeno(String keys) {

        jmeno.sendKeys(keys);

        return this;
    }

    public VlecnaHostPage fillPrijmeni(String keys) {

        prijmeni.sendKeys(keys);

        return this;
    }

    public VlecnaHostPage fillUlice(String keys) {

        ulice.sendKeys(keys);

        return this;
    }

    public VlecnaHostPage fillMesto(String keys) {

        mesto.sendKeys(keys);

        return this;
    }

    public VlecnaHostPage fillPSC(String keys) {

        PSC.sendKeys(keys);

        return this;
    }

    public VlecnaHostPage fillZeme(String keys) {

        zeme.sendKeys(keys);

        return this;
    }
}
