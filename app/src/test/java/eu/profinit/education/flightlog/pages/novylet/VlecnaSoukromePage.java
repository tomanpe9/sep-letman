package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class VlecnaSoukromePage extends ChildVlecnaPage {

    private WebElement imatrikulace;
    private WebElement typ;

    public VlecnaSoukromePage(WebDriver driver, VlecnaPage parent) {
        super(driver, parent);

        imatrikulace = driver.findElement(By.xpath("//*[@id=\"guestAirplaneImmatriculation\"]"));
        typ = driver.findElement(By.xpath("//*[@id=\"guestAirplaneType\"]"));
    }

    public VlecnaSoukromePage fillImatrikulace(String keys) {

        imatrikulace.sendKeys(keys);

        return this;
    }

    public VlecnaSoukromePage fillTyp(String keys) {

        typ.sendKeys(keys);

        return this;
    }
}
