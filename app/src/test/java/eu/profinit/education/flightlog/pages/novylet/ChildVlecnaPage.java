package eu.profinit.education.flightlog.pages.novylet;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.WebDriver;

public class ChildVlecnaPage extends ParentPage {

    private VlecnaPage parent;

    public ChildVlecnaPage(WebDriver driver, VlecnaPage parent) {
        super(driver);

        this.parent = parent;
    }

    public VlecnaPage getParent() {
        return parent;
    }
}
