package eu.profinit.education.flightlog.pages.report;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LetPage extends ParentPage {

    private ReportPage parent;

    public LetPage(WebDriver driver, ReportPage parent, WebElement tableBody) {
        super(driver);

        this.parent = parent;
    }

    public ReportPage getParent() {
        return parent;
    }
}
