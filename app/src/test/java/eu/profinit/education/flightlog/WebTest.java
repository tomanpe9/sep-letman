package eu.profinit.education.flightlog;

import eu.profinit.education.flightlog.pages.lety.LetyPage;
import eu.profinit.education.flightlog.pages.novylet.MenuPage;
import eu.profinit.education.flightlog.pages.novylet.NovyLetPage;
import eu.profinit.education.flightlog.pages.report.ReportPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = IntegrationTestConfig.class)
@Transactional
@TestPropertySource(
    locations = "classpath:application-ui-integrationtest.properties")
public class WebTest {

    private static String URL = "http://localhost:8081";

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized"); // open Browser in maximized mode
        options.addArguments("disable-infobars"); // disabling infobars
        options.addArguments("--disable-extensions"); // disabling extensions
        options.addArguments("--disable-gpu"); // applicable to windows os only
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model
        driver = new ChromeDriver(options);
        driver.get(URL);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void testFlightsPage() {
        MenuPage menuPage = new MenuPage(driver);
        menuPage.gotoLety();

        System.out.println("Title of page is: " + driver.getTitle());

        LetyPage letyPage = new LetyPage(driver);

        letyPage
            .getLety()
            .get(0)
            .clickPristat()
            .getParent()
            .getLety()
            .get(1)
            .clickVybratCasPristani()
            .getCasPristaniPage()
            .clickZrusit()
            .getParent()
            .clickVybratCasPristani()
            .getCasPristaniPage()
            .fillCasPristani("12120020181956")
            .clickPristat();

        assertEquals(0, letyPage.getLety().size());
    }

    @Test
    public void testNewFlightPage1() {
        MenuPage menuPage = new MenuPage(driver);
        menuPage.gotoNovyLet();

        System.out.println("Title of page is: " + driver.getTitle());

        NovyLetPage novyLetPage = new NovyLetPage(driver);

        novyLetPage
            .fillCasVzletu("12120020181956")
            .fillUloha("Nejaka uloha");

        novyLetPage
            .getVlecnaPage()
            .clickKlubove()
            .getKlubovePage()
            .selectLetadloKlubu(1)
            .getParent()
            .clickClenKlubu()
            .getClenKlubuPage()
            .selectPilot(5)
            .getParent()
            .selectKopilot(3)
            .fillPoznamka("Nejaka poznamka");

        novyLetPage
            .getKluzakPage()
            .clickSoukrome()
            .getSoukromePage()
            .fillImatrikulace("123456")
            .fillTyp("2")
            .getParent()
            .clickHost()
            .getHostPage()
            .fillJmeno("Jmeno")
            .fillPrijmeni("Prijmeni")
            .fillUlice("Ulice")
            .fillMesto("Mesto")
            .fillPSC("PSC")
            .fillZeme("Zeme")
            .getParent()
            .selectKopilot(4)
            .fillPoznamka("Nejaka druha poznamka");

        novyLetPage
            .clickStart()
            .acceptAlert();

        menuPage
            .gotoLety();

        LetyPage letyPage = new LetyPage(driver);

        assertTrue(letyPage.getLety().size() > 1);
    }

    @Test
    public void testNewFlightPage2() {
        MenuPage menuPage = new MenuPage(driver);
        menuPage.gotoNovyLet();

        System.out.println("Title of page is: " + driver.getTitle());

        NovyLetPage novyLetPage = new NovyLetPage(driver);

        novyLetPage
            .fillCasVzletu("12120020181956")
            .fillUloha("Nejaka uloha");

        novyLetPage
            .getVlecnaPage()
            .clickSoukrome()
            .getSoukromePage()
            .fillImatrikulace("123456")
            .fillTyp("2")
            .getParent()
            .clickHost()
            .getHostPage()
            .fillJmeno("Jmeno")
            .fillPrijmeni("Prijmeni")
            .fillUlice("Ulice")
            .fillMesto("Mesto")
            .fillPSC("PSC")
            .fillZeme("Zeme")
            .getParent()
            .selectKopilot(3)
            .fillPoznamka("Nejaka poznamka");

        novyLetPage
            .getKluzakPage()
            .clickKlubove()
            .getKlubovePage()
            .selectLetadloKlubu(1)
            .getParent()
            .clickClenKlubu()
            .getClenKlubuPage()
            .selectPilot(8)
            .getParent()
            .selectKopilot(4)
            .fillPoznamka("Nejaka druha poznamka");

        novyLetPage
            .clickStart()
            .acceptAlert();

        menuPage
            .gotoLety();

        LetyPage letyPage = new LetyPage(driver);

        assertTrue(letyPage.getLety().size() > 1);
    }

    @Test
    public void testReportPage() {
        MenuPage menuPage = new MenuPage(driver);
        menuPage.gotoReport();

        System.out.println("Title of page is: " + driver.getTitle());

        ReportPage reportPage = new ReportPage(driver);

        assertTrue(reportPage.getLety().size() > 0);
    }
}
