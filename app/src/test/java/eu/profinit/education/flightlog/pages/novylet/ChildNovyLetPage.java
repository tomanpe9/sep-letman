package eu.profinit.education.flightlog.pages.novylet;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.WebDriver;

public class ChildNovyLetPage extends ParentPage {

    private NovyLetPage parent;

    public ChildNovyLetPage(WebDriver driver, NovyLetPage parent) {
        super(driver);

        this.parent = parent;
    }

    public NovyLetPage getParent() {
        return parent;
    }
}
