package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class VlecnaKlubovePage extends ChildVlecnaPage {

    private Select letadloKlubu;

    public VlecnaKlubovePage(WebDriver driver, VlecnaPage parent) {
        super(driver, parent);

        letadloKlubu = new Select(driver.findElement(By.xpath("//*[@id=\"clubAirplaneImmatriculation\"]")));
    }

    public VlecnaKlubovePage selectLetadloKlubu(int index) {

        letadloKlubu.selectByIndex(index);

        return this;
    }
}
