package eu.profinit.education.flightlog.pages.novylet;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.WebDriver;

public class ChildKluzakPage extends ParentPage {

    private KluzakPage parent;

    public ChildKluzakPage(WebDriver driver, KluzakPage parent) {
        super(driver);

        this.parent = parent;
    }

    public KluzakPage getParent() {
        return parent;
    }
}