package eu.profinit.education.flightlog.pages.lety;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CasPristaniPage extends ParentPage {

    private LetPage parent;

    private WebElement casPristani;
    private WebElement pristat;
    private WebElement zrusit;

    public CasPristaniPage(WebDriver driver, LetPage parent) {
        super(driver);

        this.parent = parent;

        casPristani = driver.findElement(By.xpath("/html/body/router-view/div/div/div/div/div/div[2]/div[1]/input"));
        pristat = driver.findElement(By.xpath("/html/body/router-view/div/div/div/div/div/div[2]/div[2]/input[1]"));
        zrusit = driver.findElement(By.xpath("/html/body/router-view/div/div/div/div/div/div[2]/div[2]/input[2]"));
    }

    public LetPage getParent() {
        return parent;
    }

    public CasPristaniPage fillCasPristani(String keys) {

        casPristani.sendKeys(keys);

        return this;
    }

    public CasPristaniPage clickPristat() {

        pristat.click();

        return this;
    }

    public CasPristaniPage clickZrusit() {

        zrusit.click();

        return this;
    }
}
