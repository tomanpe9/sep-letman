package eu.profinit.education.flightlog.pages.report;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ReportPage extends ParentPage {

    private WebElement stahnoutLetyVCsv;
    private List<LetPage> lety;

    public ReportPage(WebDriver driver) {
        super(driver);

        stahnoutLetyVCsv = driver.findElement(By.xpath("/html/body/router-view/div/div/a"));

        lety = new ArrayList<>();

        WebElement tableBody = driver.findElement(By.xpath("/html/body/router-view/div/table/tbody"));
        List<WebElement> flights = tableBody.findElements(By.tagName("tr"));

        flights
            .forEach(item -> lety.add(new LetPage(driver, this, item)));
    }

    public ReportPage clickStahnoutLetyVCsv() {

        stahnoutLetyVCsv.click();

        return this;
    }

    public List<LetPage> getLety() {
        return lety;
    }
}
