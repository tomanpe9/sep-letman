package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class KluzakKlubovePage extends ChildKluzakPage {

    private Select letadloKlubu;

    public KluzakKlubovePage(WebDriver driver, KluzakPage parent) {
        super(driver, parent);

        letadloKlubu = new Select(driver.findElement(By.xpath("//*[@id=\"clubAirplaneImmatriculation\"]")));
    }

    public KluzakKlubovePage selectLetadloKlubu(int index) {

        letadloKlubu.selectByIndex(index);

        return this;
    }
}
