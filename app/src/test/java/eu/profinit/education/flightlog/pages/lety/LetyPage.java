package eu.profinit.education.flightlog.pages.lety;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class LetyPage extends ParentPage {

    private List<LetPage> lety;

    public LetyPage(WebDriver driver) {
        super(driver);

        lety = new ArrayList<>();

        WebElement tableBody = driver.findElement(By.xpath("/html/body/router-view/div/div/div/table/tbody"));
        List<WebElement> flights = tableBody.findElements(By.tagName("tr"));

        flights
            .forEach(item -> lety.add(new LetPage(driver, this, item)));
    }

    public List<LetPage> getLety() {
        return lety;
    }
}
