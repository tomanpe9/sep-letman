package eu.profinit.education.flightlog.pages.novylet;

import eu.profinit.education.flightlog.pages.ParentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuPage extends ParentPage {

    private WebDriver driver;

    private WebElement lety;
    private WebElement novyLet;
    private WebElement report;

    public MenuPage(WebDriver driver) {
        super(driver);

        lety = driver.findElement(By.xpath("/html/body/div/div/ul/li[1]/a"));
        novyLet = driver.findElement(By.xpath("/html/body/div/div/ul/li[2]/a"));
        report = driver.findElement(By.xpath("/html/body/div/div/ul/li[3]/a"));
    }

    public MenuPage gotoLety() {

        lety.click();

        return this;
    }

    public MenuPage gotoNovyLet() {

        novyLet.click();

        return this;
    }

    public MenuPage gotoReport() {

        report.click();

        return this;
    }
}
