package eu.profinit.education.flightlog.pages.novylet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class KluzakClenKlubuPage extends ChildKluzakPage {

    private Select pilot;

    public KluzakClenKlubuPage(WebDriver driver, KluzakPage parent) {
        super(driver, parent);

        pilot = new Select(driver.findElement(By.xpath("/html/body/router-view/div/form/compose[2]/div[2]/compose/div[3]/div/select")));
    }

    public KluzakClenKlubuPage selectPilot(int index) {

        pilot.selectByIndex(index);

        return this;
    }
}