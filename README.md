# Project documentation

## Source code

Divided into Backend and Frontend.

Skeleton downloaded from 
https://github.com/profinit/flight-log

## Backend

Complete

### Individual parts

* app - Application connector (Starter)
* common - Common parts (exceptions and Clocking)
* domain - Model and repositories
* external - External database linkage
* rest - REST services (API for frontend)
* services -  Services and TransferObjects 

### Data from external system

Data downloaded from external system 
http://vyuka.profinit.eu:8080/club/user

    {
      "memberId" 9,
      "firstName": "Miloš",
      "lastName": "Korbel",
      "roles":[
      "PILOT", "BACKOFFICE"
      ]
    }

## Frontend

Complete

## Documentation

### Offer

https://docs.google.com/document/d/1Tjyc3a80qRjCXn6x_olxNfD-cDx9pLGkOvVUW1_8VvQ/edit?usp=sharing

PDF version saved in documentation/nabidka/

### Analysis

Was not created..

### Requirements specification

https://docs.google.com/document/d/1y9GWespos_Yks3IITXiOJVzB1IoAAcC5QCo12uP1aSE/edit?usp=sharing

Remade into LaTeX version:

https://www.overleaf.com/read/xzfsdsrpzjmx

LaTeX version saved in documentation/specifikace/

### Change request - Analysis

https://www.overleaf.com/read/svccndzszrvv

LaTeX version saved in documentation/zmen-pozadavek/

### Other documents

Yet to be done