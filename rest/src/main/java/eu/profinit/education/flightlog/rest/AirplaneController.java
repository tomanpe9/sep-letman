package eu.profinit.education.flightlog.rest;

import eu.profinit.education.flightlog.service.AirplaneService;
import eu.profinit.education.flightlog.to.AirplaneTo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.profinit.education.flightlog.domain.entities.Airplane;

import java.util.ArrayList;
import java.util.stream.Collectors;

import java.util.List;

@RestController
public class AirplaneController {

    private final AirplaneService flightService;

    public AirplaneController(AirplaneService flightService) {
        this.flightService = flightService;
    }

    @RequestMapping(value = "/airplane", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<AirplaneTo> getAirplanes() {
        return flightService.getClubAirplanes();
    }
}
