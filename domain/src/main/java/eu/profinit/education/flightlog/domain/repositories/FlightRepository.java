package eu.profinit.education.flightlog.domain.repositories;

import eu.profinit.education.flightlog.domain.entities.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import eu.profinit.education.flightlog.domain.entities.Airplane;

import java.util.List;

public interface FlightRepository extends JpaRepository<Flight, Long> {

    List<Flight> findAllByLandingTimeIsNullOrderByTakeoffTimeAscIdAscFlightTypeDesc();

    List<Flight> findAllByLandingTimeIsNotNullOrderByTakeoffTimeAscIdAscFlightTypeDesc();

    List<Flight> findAllByFlightTypeOrderByTakeoffTimeAsc(Flight.Type flightType);

    default List<Flight> findAllByFlightTypeIsTowplaneOrderByTakeoffTimeAsc() {
        return findAllByFlightTypeOrderByTakeoffTimeAsc(Flight.Type.TOWPLANE);
    }
}

