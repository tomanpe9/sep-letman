package eu.profinit.education.flightlog.domain.fields;

import eu.profinit.education.flightlog.domain.JpaConstants;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class Task {

    public static final Task TOWPLANE_TASK = new Task("VLEK");

    @Getter
    @Column(name= JpaConstants.Columns.TASK)
    private String value;

    public static Task of(String value) {
        return new Task(value);
    }

}
